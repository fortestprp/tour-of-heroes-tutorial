import { Component, OnInit } from '@angular/core';
import { Router, ParamMap } from '@angular/router';

import { DataService } from './data.service';
import { Hero } from './hero';


@Component({
  selector: 'app-my-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})

export class HeroesComponent implements OnInit {

  title = 'Tour of Heroes';
  heroes: Hero[];
  selectedHero: Hero;

  constructor(private ds: DataService, private router: Router) {
  }

  ngOnInit(): void {

    this.getHeroes();
    // throw new Error('Method not implemented.');
  }

  getHeroes(): void {
    console.log('in getHeroes()');
    // this.heroes = this.ds.getHeroes();

    this.ds.getHeroesSlowly()
      .then(heros => {
        this.heroes = heros;
        console.log('Promise Fullfiled!');
      })
      .catch(err => { console.log(err); });

    console.log('out getHeroes()');
  }

  onSelect(hero: Hero): void {
    this.selectedHero = hero;
  }

  gotoDetail() {
    this.router.navigate(['/detail', this.selectedHero.id]);
  }

}

