import { Injectable } from '@angular/core';
import { Hero } from './hero';
import { HEROES } from './mock-heroes';



@Injectable()
export class DataService {
  private static GENERATED = 0;
  private selectedHero: Hero;
  private heros: Hero[];

  constructor() {
    this.generateHeros(10);
  }

  public getHeros(): Hero[] {
    return this.heros;
  }

  getHeroes(): Hero[] {
    return HEROES;
  }

  getHero(_id): Promise<Hero> {
    return Promise.resolve(HEROES.find((hero) => hero.id === _id));
  }

  getHeroesSlowly(): Promise<Hero[]> {
    return new Promise(resolve => { setTimeout(() => resolve(this.getHeroes()), 500); });
  }

  public generateHeros(howMany: number): Hero[] {
    this.heros = new Array<Hero>();
    for (let i = 0; i < howMany; i++) {
      let x = DataService.GENERATED + i;
      this.heros.push(
        { id: x, name: 'Jarvis ' + x }
      );
    }
    DataService.GENERATED += howMany;
    return this.heros;
  }

  public selectHero(hero): void {
    this.selectedHero = hero;
  }

  public getSelectedHero(): Hero { return this.selectedHero; }

}
