import { Component, OnInit } from '@angular/core';
import { Hero } from './hero';
import { DataService } from './data.service';

@Component({
  selector: 'app-my-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ]
})
export class DashboardComponent implements OnInit {

  heroes: Hero[];

  constructor(private ds: DataService) { }

  ngOnInit() {
    this.ds.getHeroesSlowly()
      .then(heroes => this.heroes = heroes.slice(2, 6))
      .catch(err => console.log(err));
    // this.heroes = this.ds.getHeroes().slice(0, 2);
  }
}
